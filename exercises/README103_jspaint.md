# Ansible Playbooks 103 - Installing JS paint

Amazing, you're getting the hang of it! 

Now let's make an Ansible playbook to instal the most important TOOL OF THEM ALL! 

YES! 

🎨🖼👨‍🎨 JS PAINT! 🎨🖼👨‍🎨

## Task

Create a playbook to instal paint JS!

https://github.com/1j01/jspaint


## Deliverables

- use your previous playbooks to install paintjs! 
- Add documentation 

🎨🖼👨‍🎨 JS PAINT for the WIN! 🎨🖼👨‍🎨
