# Playbooks and YAML

Today wel'll cover YAML and playbooks.  
Start with basics, end up in advanced playbooks and YAML. 


Objectives:
- YAML basics
- YAML list and basic data types
- Playbooks main section - host, tasks, modules 
- Ansible main modules - installing software and copying files
- Ansible temple module
- Ansible variables
- Ansible vault 


### YAML main sections 

YAML is a data serialization language. 
Largly it works a lot like JSON, which is like a dictionay in python. 

SO you have key and values. Like a dictionary in python as oposed to working with index.

You can define complex data structure with this type of language.
We'll start simple 


Check these resources out:

- https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html
- https://www.cloudbees.com/blog/yaml-tutorial-everything-you-need-get-starteds

#### Definition of string, numerical, boolean and null

Simple name and age definition, one being a string one being a number

```yaml 
name: filipe
age: 30
billionaire: false
happy: true
ownership_of_pets: null
```

#### Definition of a list 


There are several ways of defining a list in YAML.

You can use the []

```yaml
skills: ["python", "ansible", "rock climbing", "Motor cross" ]

```

Or it could be like this, using `-` in a new line:

```yaml
skills:
    - python
    - ansible
    - rock climbing
    - motocross
```

#### Definition of a dictionary

This is useful when there are other keys inside other keys! 

Let's say we have one human and lot's of data for that human.

```yaml
human_1:
        human_id: 232
        name: filipe
        age: 30
```

Above is a dictionary, `human` with 3 keys `human_id`, `name` and `age`

Let's say we have lot's of humans that are numbered and we need their name and age linked to their number.

Dictionary with a list inside. 

```yaml 
humans:
    - human_1:
        human_id: 232
        name: filipe
        age: 30
    - human_2:
        human_id: 129
        name: Zac
        age: 21
    - human_3:
        human_id: 321
        name: Rema
        age: 21
```

```python
humans = {
    [human1= {

    },
    human2={

    }]
}
```

Dictionary with a dictionay inside: 

```yaml 
humans:
    human_1:
        human_id: 232
        name: filipe
        age: 30
    human_2:
        human_id: 129
        name: Zac
        age: 21
    human_3:
        human_id: 321
        name: Rema
        age: 21
```

```python
humans = {
    human1 = {

    }, 
    human2={

    }
}
```


#### writting a blog or text with multiple lines

Sometime you just want to write a blob of text. This is useful in for example, writing some bash comand into a playbook.

Let's see an example where we give our human a haiku.

using the `|`

```yaml
human_1:
        human_id: 232
        name: filipe
        age: 30
        kaiku: |
            Chocolate - A Haiku
            by Anon
            Grainy summertime
            A more, little chocolate runs
            at the perfect kites
```

or you could use `>`

```yaml
human_1:
        human_id: 232
        name: filipe
        age: 30
        kaiku: >
            Chocolate - A Haiku
            by Anon
            Grainy summertime
            A more, little chocolate runs
            at the perfect kites
```


## Ansible Playbooks and YAML

Playbooks use yaml, but have their own documentation and structure. 

There are a few important sections.

- How to start a playbook
- defining the target/host
- defining tasks
- using modules


#### Starting a Playbook

Guide:  https://docs.ansible.com/ansible/latest/network/getting_started/first_playbook.html


Your playbooks should be written to specific hosts. 
Once you have define these in the host file, you can then write playbooks for them. 

At the start you can define:

- Name of the play 
- What hosts you want to target
- What level of permission you want to have and user 


Your playbook starts with `---`

Here is an example:

```yaml
---
- name: Filipe is a GOD and TOMATOS are the BEST FRUIT let's Install mysql in server
  hosts: db_test_servers
  remote_user: root
  become: true

```

`name` is just the name of the playbook
`hosts`is what hosts / targets you want to run the plays in
`remote_user` is what user to become inside machine
`become_true`is to run stuff as sudo

This is how you usually start a playbook. 

This is not the task, but matainformation:
    - Where should they run
    - name of the playbook
    - how should they run (permision and users)
    - NOT the actuall tasks



#### Defining tasks and using modules: 

This area, you start the section of tasks, and subsequently you write tasks you want the playbook to execute using modules from ansible library.

Modules to build tasks:

- https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_module.html
- https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html
- https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html
- https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html
- https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html
- https://docs.ansible.com/ansible/latest/collections/ansible/posix/synchronize_module.html


The followin is the syntax

```yaml 
tasks:
    - name: FILIPE TOMATO TASK
        ansible.builtin.yum:
            name: httpd
            State: present
```

Under ther task section, I have define a file with 1 item call FILIP'S TOMATO TASK.

This runs yum and install httpd.

You can list several tasks. See bellow there is one task using yum to install several packages AND a SECOND TASK making a file.

```yaml 
tasks:
    - name: FILIPE TOMATO TASK
        ansible.builtin.yum:
            name:
                - httpd
                - postgresql
                - postgresql-server
            State: present

    - name: FILIPE TOMATO DISSERTATION
        ansible.builtin.file:
            path: ~/filipe_tomato_dissertation.txt
```

Note that the first task, installing 3 softwares is still only one task.


We can now make a third task writting content into the file.

```yaml 
tasks:
    - name: FILIPE TOMATO TASK
        ansible.builtin.yum:
            name:
                - httpd
                - postgresql
                - postgresql-server
            State: present

    - name: FILIPE TOMATO DISSERTATION
        ansible.builtin.file:
            path: ~/filipe_tomato_dissertation.txt

    - name: FILIPE BASHed content into file
        ansible.builtin.shell: |
            ~/filipe_tomato_dissertation.txt << "AMAZING tomatos"
```


Putting the entire playbook together

```yaml
---
- name: Filipe is a GOD and TOMATOS are the BEST FRUIT let's Install mysql in server
  hosts: db_test_servers
  remote_user: root
  become: true

tasks:
    - name: FILIPE TOMATO TASK
        ansible.builtin.yum:
            name:
                - httpd
                - postgresql
                - postgresql-server
            State: present

    - name: FILIPE TOMATO DISSERTATION
        ansible.builtin.file:
            path: ~/filipe_tomato_dissertation.txt

    - name: FILIPE BASHed content into file
        ansible.builtin.shell: |
            ~/filipe_tomato_dissertation.txt << "AMAZING tomatos"

```



### Variables and Playbooks 

You can use variables in playbooks as well. 

Define them using the key `vars`

```yaml
vars: 
    my_fav_food: truffle
    my_fav_ip: 127.0.0.1
    fav_person: me

```


And then call them into your code using ``{{ example_var }}``

```yaml

- name: Make some file with vard
    ansible.builtin.copy:
        dest: ~/file_with_vars.html
        content: |
            <body>
                <h2>WOw I really like {{ my_fav_ip }}</h2>
                <h2>{{ my_fav_ip }} is where I get sent all my {{ my_fav_food }}</h2>
                <h2>I share {{ my_fav_food }} with {{ fav_person }}, my favorit food  and my favorit person</h2>
            </body>

```

**var from seperate file**

You can seperate your variables from your code. 
Just make a simple file and add the var in using YAML syntax. 

example 
```YAML
# /etc/ansible/var_example

fav_book: "Rich Dad Poor Dad"
secret_number: 100080047
ip_of_db: 10.32.11.1
port_id: 27017
port_web: 80
```

Then you just call the file using the `vars_files`

```yaml
- hosts: all
  remote_user: root
  vars: 
    my_fav_food: truffle
    my_fav_ip: 127.0.0.1
    fav_person: me

  vars_files:
    - /etc/ansible/var_example

```

### mdoule template 

You can also use variable inside a template.

Template is a module that allows you to call in a file but also do two things:

- jinja syntax (pythonyish)
- Intermpolate variables 

Just define a template file:


```html
<!-- example html file in  /etc/ansible/goodhtml_example.html-->
<body>
    <h2>WOw I really like {{ my_fav_ip }}</h2>
    <h2>{{ my_fav_ip }} is where I get sent all my {{ my_fav_food }}</h2>
    <h2>I share {{ my_fav_food }} with {{ fav_person }}, my favorit food  and my favorit person</h2>

    <p>More varibales  {{ ip_of_db }}</p>
    <p>More varibales {{ port_id }}</p>
    <p>More varibales {{ my_fav_food }}</p>
</body>
```

Then in your tasks call the temple
```yaml
- name: Template a file to /etc/file.conf
  ansible.builtin.template:
    src: /etc/ansible/goodhtml_example.html
    dest: /etc/file.html
```


This is great to take care of configuration files and not ruin them.