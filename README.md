# Ansible 

Configuration & Orchestration management tools.
Ansible fall in the configuration tools. 

How it works is you setup an Ansible controller and give it lists of host it can perform actions, such as intall software or confiure management. 

This connection is usaully SSH but can also be password and others. 

We'll start by install ansible in a Controller node, then make it able to ssh into the host machines machines. 


### Ansible Setup 


Create 1 Contoller node
Create 2 Host nodes

The Controller node should have:

- 4GB of ram
- 2 processors

Host nodes can be t2.micros

### Setup of Controller node

To setup please follow the following documentation:
https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-rhel-centos-or-fedora

### Setup the host node

Setup of ssh connection between Master and host

1) Generate ssh key in controller node

```
ssh-keygen -t ecdsa -b 521
```

2) add public key to ~/.ssh/authorized_keys in host node

3) re-start ssh-agent with

```
eval `ssh-agent`
```

4) Try to ssh from the controller machine to your host machine using the pivate key

### YAML - Yet another Markup Language

It is anothe markup language used to configure files.

We will get good at YMAL.

YMAL is anoying because, as a markup language it breaks with little information about where it broke.

### configuring the Host File

This file keep a list of the host to target. 
It exists in `/etc/ansible/host` if you install ansible via the binary and not via pip. 
It is written in YAML.

You can make list of host.

Syntax:

```
[name-space]
<ip.of.server.hostA> <option>
<ip.of.server.hostB> <option>

```

Example with useful breakdown and with option.

```
[webservers]
173.10.1.0 ansible_connection=ssh ansible_ssh_user=ec2-user
173.10.1.1 ansible_connection=ssh ansible_ssh_user=ec2-user
173.10.1.3 ansible_connection=ssh ansible_ssh_user=ec2-user


[dbservers]
one.example.com ansible_ssh_user=ec2-user
two.example.com ansible_ssh_user=ec2-user
three.example.com ansible_ssh_user=ec2-user

[test-servers]
one.example.com
two.example.com


[test-db-server]
one.example.com

[jenkins]
one.example.com

```

Our example
```
[hostsAlpha]
172.31.26.185 ansible_ssh_user=ec2-user

[hostB]
172.31.22.211 ansible_ssh_user=ec2-user
```

**test your connection using this**
`ansible all -m ping`

### Ansible Adhock Commands

Command you can run on command line of Ansible controller to run in your host machines!

Syntax:
```
ansible <what_group_os_hosts> -m <ansible_module>
```

First one is the ping command

```
ansible all -m ping
ansible hostA -m ping
ansible hostB -m ping
```

We can also run shell commands:

```
ansible all -m shell -a "printenv"
ansible all -m shell -a "uptime"
ansible all -m shell -a "df"
```


### Playbooks 

Playbooks are a set of tasks, aka instructions/plays, that you run agains certain hosts.

You can define where the playbook is going to run inside the playbook itself, or give it as a variable. We well define it in the playbook initially. 

The initial syntax of a playbook it:

```
---
- name: Install httpd
  hosts: hostA
  remote_user: root

```

Then you can define tasks:

```
  tasks:
  - name: instal httpd
    yum:
        name: httpd
        state: latest
```


Full Playbook 

```
---
- name: Install httpd
  hosts: hostA
  remote_user: root
  become: true

  tasks:
  - name: instal httpd
    yum:
        name: httpd
        state: latest
  
  - name: Make sure a service unit is running
    ansible.builtin.systemd:
        state: started
        name: httpd
```